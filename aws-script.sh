#!/bin/bash

STS=$(aws sts assume-role \
    --role-arn arn:aws:iam::171741190524:role/AWSCloudFormationStackSetAdministrationRole \
    --role-session-name AWSCLI-Session)

echo $STS

AWS_ACCESS_KEY_ID=$(jq -r '.Credentials.AccessKeyId' <<< $STS)
AWS_SECRET_ACCESS_KEY=$(jq -r '.Credentials.SecretAccessKey' <<< $STS)
AWS_SESSION_TOKEN=$(jq -r '.Credentials.SessionToken' <<< $STS)
AWS_DEFAULT_REGION=us-west-2

export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
export AWS_SESSION_TOKEN
export AWS_DEFAULT_REGION

aws sts get-caller-identity
